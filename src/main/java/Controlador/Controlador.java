package Controlador;

import Modelo.Bomba;
import Modelo.Gasolina;
import Vista.dlgGasolina;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener {
    private dlgGasolina vista;
    private Bomba bomba;
    private Gasolina gasolina;
    private int capacidadActualBomba;
    private int contador = 0;
    private float precioVenta = 0.0f;

    public Controlador(dlgGasolina vista, Bomba bomba, Gasolina gasolina) {
        this.vista = vista;
        this.bomba = bomba;
        this.gasolina = gasolina;

        // Configurar listeners de eventos
        vista.btnIniciar.addActionListener(this);
        vista.btnRegistrar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnReiniciar.addActionListener(this);
        vista.cbTipoGas.addActionListener(this);
        
        this.capacidadActualBomba = vista.jsInventario.getValue();
    }

    private void iniciarVista() {
        vista.setTitle("::    Gas de México   ::");
        vista.setSize(700, 550);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnIniciar) {          

                try{
                    int numBomba = Integer.parseInt(vista.txtNumBom.getText());
                    String tipoGasolina = vista.cbTipoGas.getSelectedItem().toString();
                    Gasolina gasolina = new Gasolina(numBomba, tipoGasolina, precioVenta);
                    bomba.iniciarBomba(numBomba, gasolina);

                    // Bloquear los campos de texto
                    vista.cbTipoGas.setEnabled(false);
                    vista.txtNumBom.setEnabled(false);
                    vista.btnIniciar.setEnabled(false);
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
                } catch (Exception ex2){
                     JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
                 } 
                
        }else if (e.getSource() == vista.btnRegistrar) {
                try{
                    float cantidad = Float.parseFloat(vista.txtCantidad.getText());
                    if (cantidad > capacidadActualBomba) {
                    JOptionPane.showMessageDialog(vista, "Gasolina insuficiente.");
                    return; 
                    }

                    float costo = bomba.venderGas(cantidad);

                    if (costo > 0) {
                        float ventasTotales = bomba.getVentasTotales();
                        vista.txtCosto.setText(String.valueOf(costo));
                        vista.txtTotalVenta.setText(String.valueOf(ventasTotales));

                        int capacidadBomba = vista.jsInventario.getValue();

                        if (capacidadBomba == 0) {
                            JOptionPane.showMessageDialog(vista, "Gasolina insufuciente");
                        } else {
                            capacidadBomba = capacidadActualBomba - (int) cantidad;
                            vista.jsInventario.setValue(capacidadBomba);
                            capacidadActualBomba = capacidadBomba;

                            // Incrementar el contador de ventas
                            contador += Integer.parseInt(vista.txtCantidad.getText());
                            vista.txtContador.setText(String.valueOf(contador));
                        }
                    } else {
                        JOptionPane.showMessageDialog(vista, "La cantidad que usted ingreso esta fuera de rango");
                    }
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
                } catch (Exception ex2){
                     JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
                 }   
        }else if(e.getSource()==vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", "Decide",JOptionPane.YES_NO_OPTION);
            
            if(option == JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
            
        }else if(e.getSource()==vista.btnReiniciar){
            vista.txtNumBom.setText("");
            vista.txtCantidad.setText("");
            vista.txtCosto.setText("");
            vista.txtPrecioVenta.setText("");
            vista.txtTotalVenta.setText("");
            vista.txtContador.setText("");
            
            vista.txtNumBom.setEnabled(true);
            vista.cbTipoGas.setEnabled(true);
            
            bomba.setNumBom(0);
            bomba.setVentasTotales(0);
            gasolina.setCantidad(0);
            gasolina.setIdGasolina(0);
            gasolina.setPrecio(0);
            gasolina.setTipo("");
          
        }else if(e.getSource() == vista.cbTipoGas){
             try{
                    switch(this.vista.cbTipoGas.getSelectedIndex()){
                    case 0 -> {
                        precioVenta = 25.2f;
                        vista.txtPrecioVenta.setText(String.valueOf(precioVenta)); 
                       }
                    case 1 -> {
                        precioVenta = 27.4f;
                        vista.txtPrecioVenta.setText(String.valueOf(precioVenta));
                       }
                    case 2 -> {
                        precioVenta = 30.2f;
                        vista.txtPrecioVenta.setText(String.valueOf(precioVenta));
                       }
                    }
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
                } catch (Exception ex2){
                     JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
                 } 
        }
    }

    public static void main(String[] args) {
        Bomba bomba = new Bomba();
        dlgGasolina vista = new dlgGasolina(new JFrame(), true);
         Gasolina gasolina = new Gasolina();

        Controlador controlador = new Controlador(vista, bomba, gasolina);
        controlador.iniciarVista();
    }
}
