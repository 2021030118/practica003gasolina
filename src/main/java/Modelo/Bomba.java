
package Modelo;

/**
 *
 * @author Angel
 */
public class Bomba {
    private int numBom;
    private  Gasolina gasolina;
    private float ventasTotales;
    
    public void setNumBom(int numBom){
        this.numBom = numBom;
    }
    
    public int getNumBom(){
        return numBom;
    }
        
    public void setGasolina(Gasolina gas){
        this.gasolina = gas;
    }
    
    public Gasolina getGasolina(){
        return gasolina;
    }
    
    public void iniciarBomba(int numbom, Gasolina gas){
        this.numBom = numbom;
        this.gasolina = gas;
        this.ventasTotales = 0;
    }
    
    public float venderGas(float cantidad){
         if (cantidad <= gasolina.getCantidad()) {
            float costo = cantidad * gasolina.getPrecio();
            gasolina.setCantidad(gasolina.getCantidad() - cantidad);
            ventasTotales += costo;
            return costo;
        } else {
            return 0;
        }
    }
    
     public void setVentasTotales(float ventasTotales) {
        this.ventasTotales = ventasTotales;
    }
     
     public float getVentasTotales() {
        return ventasTotales;
    }
}
