
package Modelo;
/**
 *
 * @author Angel
 */
public class Gasolina {
    private int idGasolina;
    private String tipo;
    private float precio;
    private float cantidad;
    
    public Gasolina(){}
    
    public Gasolina(Gasolina gas){
        this.idGasolina= gas.idGasolina;
        this.tipo = gas.tipo;
        this.precio = gas.precio;
        this.cantidad = gas.cantidad; 
    }
     
    public Gasolina(int id, String tipo, float precio) { 
        this.idGasolina = id;
        this.tipo = tipo;
        this.precio = precio;
        this.cantidad = 200;
    }
    
    public void setIdGasolina(int idGas){
        this.idGasolina = idGas;
    }
    
    public int getIdGasolina(){
        return idGasolina;
    }
    
     public void setTipo(String tipo){
        this.tipo = tipo;
    }
    
    public String getTipo(){
        return tipo;
    }
    
     public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public float getPrecio(){
        return precio;
    }
    
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    public float getCantidad() {
        return cantidad;
    }
}
